
# Proyecto grupo 1
### Instrucciones importantes de git
```sh
$ git checkout -b 'godie007' # para crear rama
$ git add * # para agregar cambios
$ git checkout master # para pasar de rama
$ git branch # para identificar en que rama estoy
$ git push origin 'godie007' # aplicar cambios remotos
$ git remote -v # para saber en que repositorio estoy
$ git rm -r archivo # para eliminar archivo del git
$ git branch -r # para mostrar todas las ramas del repositorio
$ git checkout -- /ruta/file.js # para recuperar un archivo borrado accidentalmente
$ git status # para ver los cambios y el estado del repositorio
$ git reset --hard # para restaurar la informacion orginal del repositorio remoto
```
## Proyecto
Para una clínica se necesita almacenar la siguiente información:

+ Users: name, email, gender, age, createdAt updatedAt AppointmentTypes: type, createdAt, updatedAt Appointments: userId, appointmentTypeId, appointmentDate, createdAt, updatedAt

En equipo se debe implementar usando lo que se ha estudiado (JavaScript, HTML, REST, mongodb).

Se espera un CRUD por cada una de las entidades: Users, AppointmentTypes, Appointments. Se deben validar las entradas de datos El estilo de la UI no es de mayor importancia Se espera que trabajen en equipo con el flujo que revisamos en el ViveLab (Feature Branch Workflow).
Documentación:

[git Documentation]: https://www.atlassian.com/git/tutorials/comparing-workflows#feature-branch-workflow
[git Documentation]
