'use strict'

exports = module.exports = function (app, mongoose) {
  var appointmentSchema = new mongoose.Schema({
    userId: String,
    appointmentTypeId: String,
    appointmentDate: Date,
    createdAt: String,
    updateAt: String
  })
    appointmentSchema.set('autoIndex', (app.get('env') === 'development'))
    app.db.model('appointment', appointmentSchema)
}