'use strict'
/** 
* @autor:godie007
* @date: 2017/07/23 19:49
* Implementación del metodo POST para la coleccion de usuarios
**/
exports.ingresar = (req, res) => {
  const item = req.body
  console.log(item)
  req.app.db.models.user.create(item, (err, data) => {
    if (err) {
      console.log('fallo al ingresar! ' + err)
      res.send(['ERROR'])
    } else {
      res.send(['OK'])
    }
  })
}
/** 
* @autor:godie007
* @date: 2017/07/23 19:55
* implmentación del metodo GET para la coleccion de usuarios
**/
exports.mostrar = (req, res) => {
  res.render('AppointmentTypes/vista', {'params': {'title': 'Ejemplo', 'head': 'Tipos de Citas'}})
}

exports.listar = (req, res) => {
  req.app.db.models.user.find({}, (err, data) =>{
    if (err) {
      res.send(err)
    } else {
      res.send(data)
    }
  })
}
exports.buscar = (req, res) => {
  const id = req.params.id;
  req.app.db.models.user.findOne({'_id':id}, (err, data) =>{
    if (err) {
      res.send(err)
    } else {
      res.send(data)
    }
  })
}
exports.borrar = (req, res) => {
  const id = req.body.id
  console.log('id', id)
  req.app.db.models.user.remove({'_id': id}, (err, resp) =>{
    if (err) {
      res.send(['ERROR'])
    } else {
      res.send(['OK'])
    }
  })
}
exports.actualizar = (req, res) => {

}
